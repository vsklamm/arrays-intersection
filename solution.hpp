#ifndef SOLUTION_HPP_
#define SOLUTION_HPP_

#include <algorithm>
#include <cstddef>
#include <limits>
#include <unordered_set>
#include <vector>

#include <ext/pb_ds/assoc_container.hpp>

const std::size_t CACHE_FRIENDLY_SIZE = 2048;
const std::size_t SMALL_SIZE = 64;

uint32_t merge_based(const std::vector<int> &first, const std::vector<int> &second)
{
	auto bigger = first;
	std::sort(bigger.begin(), bigger.end());
	auto smaller = second;
	std::sort(smaller.begin(), smaller.end());
	std::size_t i = 0, j = 0;
	uint32_t count = 0;
	while (j < smaller.size() && i < bigger.size())
	{
		if (bigger[i] < smaller[j])
		{
			++i;
		}
		else if (bigger[i] > smaller[j])
		{
			++j;
		}
		else
		{
			++count;
			++i;
			++j;
		}
	}
	return count;
}

uint32_t binsearch_in_small(const std::vector<int> &bigger, const std::vector<int> &smaller)
{
	auto smaller_sorted = smaller;
	std::sort(smaller_sorted.begin(), smaller_sorted.end());
	uint32_t count = 0;
	for (const auto x : bigger)
	{
		if (std::binary_search(smaller_sorted.begin(), smaller_sorted.end(), x))
		{
			++count;
		}
	}
	return count;
}

uint32_t hashset_optimized(const std::vector<int> &bigger, const std::vector<int> &smaller)
{
	const auto minmax = std::minmax_element(bigger.begin(), bigger.end());
	const auto bmin = *minmax.first;
	const auto bmax = *minmax.second;
	std::unordered_set<int> table;
	for (const auto x : smaller)
	{
		if (bmin <= x && x <= bmax)
		{
			table.insert(x);
		}
	}
	uint32_t count = 0;
	for (const auto x : bigger)
	{
		count += table.erase(x);
	}
	return count;
}

uint32_t my_solution(const std::vector<int> &first, const std::vector<int> &second)
{
	const auto& bigger = first.size() > second.size() ? first : second;
	const auto& smaller = first.size() <= second.size() ? first : second;
	if (smaller.size() <= SMALL_SIZE)
	{
		return binsearch_in_small(bigger, smaller);
	}
	if (smaller.size() * 10 >= bigger.size() || bigger.size() <= CACHE_FRIENDLY_SIZE)
	{
		return merge_based(bigger, smaller);
	}
	return hashset_optimized(bigger, smaller);
}

#ifdef __GNUC__
uint32_t gnu_hashtable_based(const std::vector<int> &first, const std::vector<int> &second)
{
	const auto& bigger = first.size() > second.size() ? first : second;
	const auto& smaller = first.size() <= second.size() ? first : second;
	__gnu_pbds::gp_hash_table<int, __gnu_pbds::null_type> table;
	for (const auto x : smaller)
	{
		table.insert(x);
	}
	uint32_t count = 0;
	for (const auto x : bigger)
	{
		if (table.find(x) != table.end())
		{
			++count;
		}
	}
	return count;
}
#endif

#endif // SOLUTION_HPP_
