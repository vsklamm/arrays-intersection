#include <algorithm>
#include <cstddef>
#include <iostream>
#include <limits>
#include <vector>

#include <gtest/gtest.h>
#include "test_generator_utils.hpp"
#include "solution.hpp"

namespace
{

uint32_t std_set_intersection(std::vector<int> a, std::vector<int> b)
{
    std::sort(a.begin(), a.end());
    std::sort(b.begin(), b.end());
    std::vector<int> intersection;
    std::set_intersection(a.begin(), a.end(),
                          b.begin(), b.end(),
                          std::back_inserter(intersection));
    return uint32_t(intersection.size());
}

}

void test_with_swap(const std::vector<int> &f, const std::vector<int> &s)
{
    const auto exp1 = my_solution(f, s);
    const auto exp2 = my_solution(s, f);
    const auto act = std_set_intersection(f, s);
    EXPECT_EQ(exp1, exp2);
    EXPECT_EQ(exp1, act);
}

TEST(Correctness, Empty)
{
    const std::vector<int> f, s;
    const auto exp = std_set_intersection(f, s);
    const auto act = my_solution(f, s);
    EXPECT_EQ(exp, act);
}

TEST(Correctness, One)
{
    std::vector<int> f = { 42 }, s = { 42 };
    const auto exp = std_set_intersection(f, s);
    const auto act = my_solution(f, s);
    f = { 43 }, s = { 42 };
    const auto exp1 = std_set_intersection(f, s);
    const auto act1 = my_solution(f, s);
    EXPECT_EQ(exp, act);
    EXPECT_EQ(exp1, act1);
}

TEST(Correctness, SwapTrick)
{
    const std::vector<int>
            f = { 1 },
            s = { 5, 4, 3, 2, 1 };
    test_with_swap(f, s);
}

TEST(Correctness, Equal)
{
    const std::vector<int>
            f = { 1, 2, 3, 4, 5 },
            s = { 1, 2, 3, 4, 5 };
    const auto exp = my_solution(f, s);
    const auto act = std_set_intersection(f, s);
    EXPECT_EQ(exp, act);
}

TEST(Correctness, Reversed)
{
    const std::vector<int>
            f = { 1, 2, 3, 4, 5 },
            s = { 5, 4, 3, 2, 1 };
    test_with_swap(f, s);
}

TEST(Correctness, NonIntersecting)
{
    const std::vector<int>
            f = { 5, 4, 3, 2, 1 },
            s = { -1, -2, -3, -4, -5 };
    test_with_swap(f, s);
}

TEST(Correctness, Unsorted)
{
    const std::vector<int>
            f = { 5, 2, 1, 8, 3, 7 },
            s = { -1, 2, -3, -2, 1, 3 };
    test_with_swap(f, s);
}

TEST(Correctness, EdgeInts)
{
    const std::vector<int>
            f = { std::numeric_limits<int>::min(), 42, 666, 0 },
            s = { std::numeric_limits<int>::min(), 451, 0, std::numeric_limits<int>::max() };
   test_with_swap(f, s);
}

TEST(Correctness, RandomSameSize)
{
    using namespace gen_utils;
    std::vector<int> f, s;
    generate_vectors(f, s, { { 1e4, 1e4 }, { 1, 1 }, { 1, 1 }, RANDOM });
    test_with_swap(f, s);
}

TEST(Correctness, RandomDiffSizeHalfCommon)
{
    using namespace gen_utils;
    std::vector<int> f, s;
    generate_vectors(f, s, { { 8e2, 8e2 }, { 1e3, 1e3 }, { 0.5, 0.5 }, RANDOM });
    test_with_swap(f, s);
}

TEST(Correctness, RandomDiffSizeRCommon)
{
    using namespace gen_utils;
    std::vector<int> f, s;
    generate_vectors(f, s, { { 8e2, 8e2 }, { 1e3, 1e3 }, { 0.1, 1 }, RANDOM });
    test_with_swap(f, s);
}

TEST(Correctness, DiffCommonPosition)
{
    using namespace gen_utils;
    std::vector<int> f, s;
    generate_vectors(f, s, { { 8e2, 8e2 }, { 5e2, 1e3 }, { 0.1, 1 }, BEG_BEG });
    test_with_swap(f, s);
    generate_vectors(f, s, { { 8e2, 8e2 }, { 5e2, 1e3 }, { 0.1, 1 }, BEG_END });
    test_with_swap(f, s);
    generate_vectors(f, s, { { 8e2, 8e2 }, { 5e2, 1e3 }, { 0.1, 1 }, END_BEG });
    test_with_swap(f, s);
    generate_vectors(f, s, { { 8e2, 8e2 }, { 5e2, 1e3 }, { 0.1, 1 }, END_END });
    test_with_swap(f, s);
}

TEST(Correctness, FullRandom)
{
    using namespace gen_utils;
    std::vector<int> f, s;
    generate_vectors(f, s, { { 1, 1e3 }, { 1, 1e3 }, { 0.01, 1 }, RANDOM });
    test_with_swap(f, s);
}

GTEST_API_ int main(int argc, char * argv[])
{
    std::cout << "Running main() from common_ints_testing.cpp" << std::endl;
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
