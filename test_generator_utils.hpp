#ifndef GENERATOR_UTILS_
#define GENERATOR_UTILS_

#include <algorithm>
#include <cassert>
#include <iterator>
#include <limits>
#include <random>
#include <set>
#include <unordered_set>

namespace gen_utils
{

    using pvv = std::pair<std::vector<int>, std::vector<int>>;

    std::random_device rd;
    std::mt19937 g{ rd() };
    std::size_t rand_number(std::size_t from, std::size_t to)
    {
        return std::uniform_int_distribution<std::size_t>(from, to)(g);
    }

    enum COMMON_POSITION // position of common elements in SORTED arrays
    {
        BEG_BEG, // beginnings of both arrays
        END_END, // endings of both arrays
        BEG_END, // beginning of small, ending of big
        END_BEG, // beginning of big, ending of small
        RANDOM
    };

    /**
             * @brief Контейнер для параметров генератора
             * small_s  Диапазон размера малого массива
             * coeff    Диапазон коэффициента разницы между малым и большим массивом. Точность 10^-6
             * perc     Диапазон процента общих элементов относительно малого массива. perc = [0, 1]. Точность 10^-6
             * common_pos Позиция общих элементов в массивах
             */
    struct gen_parameters
    {
        std::pair<std::size_t, std::size_t> small_s;
        std::pair<double, double> coeff;
        std::pair<double, double> perc;
        COMMON_POSITION common_pos;
    };

    /**
             * @brief Генерирует два массива - малый и большой:
             * заданного размера, количества общих элементов и их положения в каждом массиве.
             * Общие элементы могут идти подряд в начале, в конце или могут быть рандомно раскиданы по массиву.
             * Учитывается зависимость генерируюмых чисел от положения общих элементов
             * Гарантирует заданный процент содержания общих элементов в массивах
             *
             * @param small Малый массив
             * @param big Большой массив
             * @param params Контейнер с параметрами
             */
    void generate_vectors(std::vector<int> &small, std::vector<int> &big, const gen_parameters &params)
    {
        assert(params.small_s.first < (1ULL << 31) && params.small_s.second < (1ULL << 31));
        assert(1 <= params.coeff.first && 1 <= params.coeff.second);
        assert(0 <= params.perc.first && 0 <= params.perc.second && params.perc.first <= 1 && params.perc.second <= 1);

        const std::size_t sm_sz = rand_number(params.small_s.first, params.small_s.second);
        const std::size_t bg_sz = sm_sz * rand_number(params.coeff.first * 1e6, params.coeff.second * 1e6) / 1e6;
        const std::size_t common_count = sm_sz * rand_number(params.perc.first * 1e6, params.perc.second * 1e6) / 1e6;
        assert(bg_sz < (1ULL << 31) && common_count < (1ULL << 31));

        int common_min = 0;
        std::pair<int, int> small_unique, big_unique;
        switch (params.common_pos) // position of common numbers in arrays
        {
        case BEG_BEG:
            common_min = std::numeric_limits<int>::min();
            small_unique = { common_min + common_count + 1, std::numeric_limits<int>::max() };
            big_unique = small_unique;
            break;
        case END_END:
            common_min = std::numeric_limits<int>::max() - common_count;
            small_unique = { std::numeric_limits<int>::min(), common_min - 1 };
            big_unique = small_unique;
            break;
        case BEG_END:
        {
            const int mid = (bg_sz + sm_sz - common_count) / 2;
            common_min = (std::numeric_limits<int>::max() / std::max(mid, 1)) * (int(bg_sz - common_count) - mid);
            small_unique = { common_min + common_count + 1, std::numeric_limits<int>::max() };
            big_unique = { std::numeric_limits<int>::min(), common_min - 1 };
            break;
        }
        case END_BEG:
        {
            const int mid = (bg_sz + sm_sz - common_count) / 2;
            common_min = (std::numeric_limits<int>::max() / std::max(mid, 1)) * -(int(bg_sz - common_count) - mid);
            small_unique = { std::numeric_limits<int>::min(), common_min - 1 };
            big_unique = { common_min + common_count + 1, std::numeric_limits<int>::max() };
            break;
        }
        case RANDOM:
            small_unique = { std::numeric_limits<int>::min(), std::numeric_limits<int>::max() };
            big_unique = { std::numeric_limits<int>::min(), std::numeric_limits<int>::max() };
            break;
        }

        small.clear(); big.clear();
        small.resize(sm_sz); big.resize(bg_sz);
        std::uniform_int_distribution<int> common_rand(std::numeric_limits<int>::min(), std::numeric_limits<int>::max());
        std::unordered_set<int> common_set;
        if (params.common_pos != RANDOM)
        {
            for (std::size_t i = 0; i < common_count; ++i)
            {
                small[i] = common_min + int(i);
                big[i] = common_min + int(i);
            }
        }
        else
        {
            common_set.reserve(common_count);
            while (common_set.size() != common_count)
            {
                common_set.insert(common_rand(g));
            }
        }
        std::unordered_set<int> small_set_unique;
        small_set_unique.reserve(sm_sz - common_count);
        std::uniform_int_distribution<int> small_rand(small_unique.first, small_unique.second);
        while (small_set_unique.size() != sm_sz - common_count)
        {
            const int x = small_rand(g);
            if (common_set.find(x) == common_set.end())
            {
                small_set_unique.insert(x);
            }
        }
        std::set<int> big_set_unique;
        std::uniform_int_distribution<int> big_rand(big_unique.first, big_unique.second);
        while (big_set_unique.size() != bg_sz - common_count)
        {
            const int x = big_rand(g);
            if (common_set.find(x) == common_set.end() && small_set_unique.find(x) == small_set_unique.end())
            {
                big_set_unique.insert(x);
            }
        }
        if (params.common_pos == RANDOM)
        {
            std::copy(common_set.begin(), common_set.end(), small.begin());
        }
        std::copy(small_set_unique.begin(), small_set_unique.end(), small.begin() + common_count);
        std::unordered_set<int>().swap(small_set_unique);
        std::copy(common_set.begin(), common_set.end(), big.begin());
        std::unordered_set<int>().swap(common_set);
        std::copy(big_set_unique.begin(), big_set_unique.end(), big.begin() + common_count);
        std::set<int>().swap(big_set_unique);

        std::random_shuffle(small.begin(), small.end());
        std::random_shuffle(big.begin(), big.end());
    }

}

#endif // GENERATOR_UTILS_
