﻿#include <algorithm>
#include <ctime>
#include <iomanip>
#include <iostream>
#include <functional>
#include <limits>
#include <string>

#include "test_generator_utils.hpp"
#include "solution.hpp"
#include "analogues.hpp"

using namespace gen_utils;

struct solution
{
    const std::string name;
    std::pair<uint32_t, std::clock_t> result;
    std::clock_t all_clocks;
    double average_time;

    solution(std::string name)
        : name(std::move(name)), result({ 0, 0 }), all_clocks(0), average_time(0)
    {}

    void reset()
    {
        all_clocks = 0;
        average_time = 0;
    }
};

std::vector<gen_parameters> tests;

std::vector<solution> algorithms = {
    {"binsearch"},
    {"my solution"},
    {"merge"},
    {"hash"},
    {"set intersect"}
    #ifdef __GNUC__
    // ,{"gp hashtable"}
    #endif
};

std::pair<uint32_t, std::clock_t> run_solution(const std::function<uint32_t()> &func)
{
    const std::clock_t start = clock();
    const auto ans = func();
    return { ans, clock() - start };
}

const int ROUNDS = 3;

int main()
{
    // Adding tests
    for (std::size_t i = 8; i <= (1 << 20); i *= 24)
    {
        for (std::size_t j = 1; j <= (1 << 20); j <<= 5)
        {
            if (256 < i * j && i * j <= 5000000)
            {
                tests.push_back({ { i, i }, { j, j }, { 0.33, 0.33 }, BEG_BEG });
                tests.push_back({ { i, i }, { j, j }, { 0.33, 0.33 }, END_BEG });
                tests.push_back({ { i, i }, { j, j }, { 0.33, 0.33 }, RANDOM });
            }
        }
    }
    tests.push_back({ { 1e5, 1e5 }, { 1, 1 }, { 0, 0 }, RANDOM });
    tests.push_back({ { 3e5, 3e5 }, { 1, 1 }, { 0.5, 0.5 }, RANDOM });
    tests.push_back({ { 3e5, 3e5 }, { 1.2, 1.2 }, { 0.5, 0.5 }, RANDOM });
    tests.push_back({ { 3e5, 3e5 }, { 2, 2 }, { 0.5, 0.5 }, RANDOM });

    std::cout << "Running " << ROUNDS << " rounds of " << tests.size()
              << " tests with " << algorithms.size() << " algorithms." << std::endl;
    for (std::size_t test_num = 0; test_num < tests.size(); ++test_num)
    {
        std::vector<int> first, second;
        generate_vectors(first, second, tests[test_num]);
        std::cout << "Test case #" << test_num + 1 << " ("
                  << first.size() << ", "
                  << second.size() << ", " << tests[test_num].common_pos << ")" << std::endl;
        for (int round = 1; round <= ROUNDS; ++round)
        {
            algorithms[0].result = run_solution(std::bind(binsearch_based, first, second));
            algorithms[1].result = run_solution(std::bind(my_solution, first, second));
            algorithms[2].result = run_solution(std::bind(merge_based, first, second));
            algorithms[3].result = run_solution(std::bind(hashset_based, first, second));
            algorithms[4].result = run_solution(std::bind(std_set_intersection, first, second));
#ifdef __GNUC__
            // algorithms[5].result = run_solution(std::bind(gnu_hashtable_based, first, second));
#endif
            for (auto& algo : algorithms)
            {
                algo.all_clocks += algo.result.second;
            }
            for (std::size_t s = 1; s < algorithms.size(); ++s)
            {
                if (algorithms[s].result.first != algorithms[s - 1].result.first)
                {
                    std::cerr << "Wrong answer at test #" << test_num + 1
                              << " in algo " << algorithms[s].name
                              << " or " << algorithms[s - 1].name << std::endl;
                    return 0;
                }
            }
        }
        double best_time = std::numeric_limits<double>::max();
        for (auto& algo : algorithms)
        {
            algo.average_time = double(algo.all_clocks) / ROUNDS / CLOCKS_PER_SEC;
            best_time = std::min(best_time, algo.average_time);
        }
        std::cout << std::fixed << std::setprecision(6);
        for (auto& algo : algorithms)
        {
            std::cout << algo.name << std::setw(16 - algo.name.size())
                      << "(" << algo.average_time << " s.)" << ((algo.average_time == best_time) ? "<-" : "") << std::endl;
            algo.reset();
        }
    }
}
