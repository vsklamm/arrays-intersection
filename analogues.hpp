#ifndef ANALOGUES_HPP_
#define ANALOGUES_HPP_

#include <algorithm>
#include <cstddef>
#include <unordered_set>
#include <vector>

uint32_t binsearch_based(const std::vector<int> &first, const std::vector<int> &second)
{
    auto bigger = first;
    std::sort(bigger.begin(), bigger.end());
    auto smaller = second;
    std::sort(smaller.begin(), smaller.end());
    if (smaller.size() > bigger.size())
    {
        smaller.swap(bigger);
    }
    if (smaller.empty() || bigger.empty() ||
            bigger.back() < smaller.front() || smaller.back() < bigger.front())
    {
        return 0;
    }
    uint32_t count = 0;
    auto smit = std::lower_bound(smaller.begin(), smaller.end(), bigger.front());
    auto it = bigger.begin();
    auto end = std::upper_bound(it, bigger.end(), smaller.back());
    for (; smit != smaller.end(); ++smit)
    {
        it = std::lower_bound(it, end, *smit);
        if (it != bigger.end() && *smit == *it)
        {
            ++count;
        }
    }
    return count;
}

uint32_t hashset_based(const std::vector<int> &first, const std::vector<int> &second)
{
    const auto& bigger = first.size() > second.size() ? first : second;
    const auto& smaller = first.size() <= second.size() ? first : second;
    std::unordered_set<int> table(smaller.begin(), smaller.end());
    uint32_t count = 0;
    for (const auto x : bigger)
    {
        if (table.find(x) != table.end())
        {
            ++count;
        }
    }
    return count;
}

uint32_t std_set_intersection(std::vector<int> first, std::vector<int> second)
{
    std::sort(first.begin(), first.end());
    std::sort(second.begin(), second.end());
    std::vector<int> intersection;
    std::set_intersection(first.begin(), first.end(),
                          second.begin(), second.end(),
                          std::back_inserter(intersection));
    return uint32_t(intersection.size());
}

uint32_t galloping_search(const std::vector<int> &first, const std::vector<int> &second)
{
    auto bigger = first;
    std::sort(bigger.begin(), bigger.end());
    auto smaller = second;
    std::sort(smaller.begin(), smaller.end());
    if (smaller.size() > bigger.size())
    {
        smaller.swap(bigger);
    }
    if (smaller.empty() || bigger.empty() ||
            bigger.back() < smaller.front() || smaller.back() < bigger.front())
    {
        return 0;
    }
    uint32_t pows2[std::numeric_limits<uint32_t>::digits - 1];
    for (uint32_t i = 0; i < std::extent<decltype(pows2)>::value; ++i)
    {
        pows2[i] = 1U << i;
    }
    uint32_t count = 0, i = 0;
    for (const auto x : smaller)
    {
        uint32_t k = 0;
        std::size_t i2k;
        while ((i2k = i + pows2[k]) < bigger.size() && x > bigger[i2k])
        {
            ++k;
        }
        auto it = std::lower_bound(
                    bigger.begin() + i,
                    bigger.begin() + std::min(i2k, bigger.size()),
                    x);
        if (it != bigger.end() && *it == x)
        {
            ++count;
        }
        else
        {
            i = it - bigger.begin();
        }
    }
    return count;
}

#endif // ANALOGUES_HPP_
